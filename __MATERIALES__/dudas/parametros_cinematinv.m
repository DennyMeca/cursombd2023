% Es el archivo parametros_fase3b.m
Ensamblaje_simscape_DataFile
[robot, importInfo]=importrobot('Ensamblaje_simscape.slx')
 
config=homeConfiguration(robot);

% Posicion 1, correspondiente a cierta posición articular:
config(1).JointPosition = 0;
config(2).JointPosition = 0;
% posición del cdg del body4 en cartesianos
posic1=tform2trvec(getTransform(robot,config,'Body4'))

% Posicion 2, correspondiente a cierta posición articular:
config(1).JointPosition = 0;
config(2).JointPosition = pi/8;
posic2=tform2trvec(getTransform(robot,config,'Body4'))

% Posicion 3, correspondiente a cierta posición articular:
config(1).JointPosition = 0;
config(2).JointPosition = pi/4;
posic3=tform2trvec(getTransform(robot,config,'Body4'))

% Posicion 4, correspondiente a cierta posición articular:
config(1).JointPosition = 0;
config(2).JointPosition = pi/2;
posic4=tform2trvec(getTransform(robot,config,'Body4'))

x=[posic1(1) posic2(1) posic3(1) posic4(1)]
y=[posic1(2) posic2(2) posic3(2) posic4(2)]
z=[posic1(3) posic2(3) posic3(3) posic4(3)]

tiemposim=linspace(0,20,4)