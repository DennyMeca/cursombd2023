% Simscape(TM) Multibody(TM) version: 7.2

% This is a model data file derived from a Simscape Multibody Import XML file using the smimport function.
% The data in this file sets the block parameter values in an imported Simscape Multibody model.
% For more information on this file, see the smimport function help page in the Simscape Multibody documentation.
% You can modify numerical values, but avoid any other changes to this file.
% Do not add code to this file. Do not edit the physical units shown in comments.

%%%VariableName:smiData


%============= RigidTransform =============%

%Initialize the RigidTransform structure array by filling in null values.
smiData.RigidTransform(9).translation = [0.0 0.0 0.0];
smiData.RigidTransform(9).angle = 0.0;
smiData.RigidTransform(9).axis = [0.0 0.0 0.0];
smiData.RigidTransform(9).ID = '';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(1).translation = [-2.7531406116092652 -183.99071925754058 144.99999999999997];  % mm
smiData.RigidTransform(1).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(1).axis = [0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(1).ID = 'B[VIGA_ANTEBRAZO_OPTI-1:-:CREMALLERA-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(2).translation = [-137.91063819121445 -7.9999999999988125 6.7950306641949965];  % mm
smiData.RigidTransform(2).angle = 2.0943951023931979;  % rad
smiData.RigidTransform(2).axis = [0.5773502691896264 -0.57735026918962407 -0.57735026918962684];
smiData.RigidTransform(2).ID = 'F[VIGA_ANTEBRAZO_OPTI-1:-:CREMALLERA-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(3).translation = [-75.999999999999957 2.2999999999999963 -6.0000000000000036];  % mm
smiData.RigidTransform(3).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(3).axis = [0.57735026918962584 0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(3).ID = 'B[CREMALLERA-1:-:UNION_PINZA-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(4).translation = [-3.0553337637684308e-13 9.0000000000001137 -2.3270274596143281e-13];  % mm
smiData.RigidTransform(4).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(4).axis = [-0.57735026918962584 -0.57735026918962584 -0.57735026918962584];
smiData.RigidTransform(4).ID = 'F[CREMALLERA-1:-:UNION_PINZA-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(5).translation = [0 20.329449153000056 192.49999999999994];  % mm
smiData.RigidTransform(5).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(5).axis = [-0.57735026918962584 -0.57735026918962584 -0.57735026918962584];
smiData.RigidTransform(5).ID = 'B[VIGA_HOMBRO_OPTI-1:-:VIGA_ANTEBRAZO_OPTI-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(6).translation = [10.246859388388964 20.009280742459442 9.9999999999996039];  % mm
smiData.RigidTransform(6).angle = 2.0943951023931957;  % rad
smiData.RigidTransform(6).axis = [-0.57735026918962573 -0.57735026918962573 -0.57735026918962562];
smiData.RigidTransform(6).ID = 'F[VIGA_HOMBRO_OPTI-1:-:VIGA_ANTEBRAZO_OPTI-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(7).translation = [-165.49999999999997 -1.2654807757250808e-12 341.99999999999994];  % mm
smiData.RigidTransform(7).angle = 3.1415926535897896;  % rad
smiData.RigidTransform(7).axis = [-1 -0 -0];
smiData.RigidTransform(7).ID = 'B[BASE-1:-:VIGA_HOMBRO_OPTI-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(8).translation = [-0.42372881399978723 31.329449152999661 15.000000000000018];  % mm
smiData.RigidTransform(8).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(8).axis = [0.57735026918962562 -0.57735026918962562 0.57735026918962595];
smiData.RigidTransform(8).ID = 'F[BASE-1:-:VIGA_HOMBRO_OPTI-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(9).translation = [13.778909007258269 3.6498429675791337 190.00015461646115];  % mm
smiData.RigidTransform(9).angle = 0;  % rad
smiData.RigidTransform(9).axis = [0 0 0];
smiData.RigidTransform(9).ID = 'RootGround[BASE-1]';


%============= Solid =============%
%Center of Mass (CoM) %Moments of Inertia (MoI) %Product of Inertia (PoI)

%Initialize the Solid structure array by filling in null values.
smiData.Solid(5).mass = 0.0;
smiData.Solid(5).CoM = [0.0 0.0 0.0];
smiData.Solid(5).MoI = [0.0 0.0 0.0];
smiData.Solid(5).PoI = [0.0 0.0 0.0];
smiData.Solid(5).color = [0.0 0.0 0.0];
smiData.Solid(5).opacity = 0.0;
smiData.Solid(5).ID = '';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(1).mass = 0.07303838967680279;  % kg
smiData.Solid(1).CoM = [58.913595229957984 3.0498718093383728 -5.9999999999999956];  % mm
smiData.Solid(1).MoI = [7.9778921589153855 262.88189988230499 259.00117154461617];  % kg*mm^2
smiData.Solid(1).PoI = [0 0 -1.0739955685241489];  % kg*mm^2
smiData.Solid(1).color = [0.69803921568627447 0.69803921568627447 0.69803921568627447];
smiData.Solid(1).opacity = 1;
smiData.Solid(1).ID = 'CREMALLERA*:*Predeterminado';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(2).mass = 0.21641171348412816;  % kg
smiData.Solid(2).CoM = [-0.44654702223611664 2.3261937700676807 112.09900397227059];  % mm
smiData.Solid(2).MoI = [1000.2796810701212 896.32620262448108 155.90117358911405];  % kg*mm^2
smiData.Solid(2).PoI = [-81.951210625581282 0.12996368849631079 0.06115650319368067];  % kg*mm^2
smiData.Solid(2).color = [0.69803921568627447 0.69803921568627447 0.69803921568627447];
smiData.Solid(2).opacity = 1;
smiData.Solid(2).ID = 'VIGA_HOMBRO_OPTI*:*Predeterminado';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(3).mass = 0.012325317242387465;  % kg
smiData.Solid(3).CoM = [1.3492949382104445e-08 2.7169768450287548 0];  % mm
smiData.Solid(3).MoI = [1.8763033553512545 3.6818840714548293 1.8762799090844444];  % kg*mm^2
smiData.Solid(3).PoI = [0 1.7922911913167375e-06 -3.2319132892403177e-08];  % kg*mm^2
smiData.Solid(3).color = [0.69803921568627447 0.69803921568627447 0.69803921568627447];
smiData.Solid(3).opacity = 1;
smiData.Solid(3).ID = 'UNION_PINZA*:*Predeterminado';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(4).mass = 0.15066712500115439;  % kg
smiData.Solid(4).CoM = [10.129330415065029 -4.6637289207968848 80.005923692703135];  % mm
smiData.Solid(4).MoI = [395.54943297803442 397.2735425403053 46.945517346541145];  % kg*mm^2
smiData.Solid(4).PoI = [3.7751630412044377 0.46458649876279579 -0.011534794369872736];  % kg*mm^2
smiData.Solid(4).color = [0.69803921568627447 0.69803921568627447 0.69803921568627447];
smiData.Solid(4).opacity = 1;
smiData.Solid(4).ID = 'VIGA_ANTEBRAZO_OPTI*:*Predeterminado';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(5).mass = 1.6095274837202986;  % kg
smiData.Solid(5).CoM = [-164.4773695101604 -0.00023218580666806859 138.53933960888034];  % mm
smiData.Solid(5).MoI = [28836.765735575649 30025.922688512917 4664.6624196652101];  % kg*mm^2
smiData.Solid(5).PoI = [0.002982858052004005 1792.493924675488 0.0010747683718179009];  % kg*mm^2
smiData.Solid(5).color = [0.69803921568627447 0.69803921568627447 0.69803921568627447];
smiData.Solid(5).opacity = 1;
smiData.Solid(5).ID = 'BASE*:*Predeterminado';


%============= Joint =============%
%X Revolute Primitive (Rx) %Y Revolute Primitive (Ry) %Z Revolute Primitive (Rz)
%X Prismatic Primitive (Px) %Y Prismatic Primitive (Py) %Z Prismatic Primitive (Pz) %Spherical Primitive (S)
%Constant Velocity Primitive (CV) %Lead Screw Primitive (LS)
%Position Target (Pos)

%Initialize the PrismaticJoint structure array by filling in null values.
smiData.PrismaticJoint(1).Pz.Pos = 0.0;
smiData.PrismaticJoint(1).ID = '';

smiData.PrismaticJoint(1).Pz.Pos = 0;  % m
smiData.PrismaticJoint(1).ID = '[VIGA_ANTEBRAZO_OPTI-1:-:CREMALLERA-1]';


%Initialize the RevoluteJoint structure array by filling in null values.
smiData.RevoluteJoint(3).Rz.Pos = 0.0;
smiData.RevoluteJoint(3).ID = '';

smiData.RevoluteJoint(1).Rz.Pos = 179.36290714361377;  % deg
smiData.RevoluteJoint(1).ID = '[CREMALLERA-1:-:UNION_PINZA-1]';

smiData.RevoluteJoint(2).Rz.Pos = -76.045471251760105;  % deg
smiData.RevoluteJoint(2).ID = '[VIGA_HOMBRO_OPTI-1:-:VIGA_ANTEBRAZO_OPTI-1]';

smiData.RevoluteJoint(3).Rz.Pos = 13.31743589185337;  % deg
smiData.RevoluteJoint(3).ID = '[BASE-1:-:VIGA_HOMBRO_OPTI-1]';

